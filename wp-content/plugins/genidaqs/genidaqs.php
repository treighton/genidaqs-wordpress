<?php
/*
Plugin Name:  Genidaqs Custom Functionality
Plugin URI:   www.genidaqs.com
Description:  Basic functionality
Version:      1
Author:       Treighton
Author URI:   https://tr8onthe.rocks/
License:      GPL2
License URI:  https://www.gnu.org/licenses/gpl-2.0.html
Text Domain:  genidaqs
*/

class Genidaqs {

	function __construct() {
		add_action('init', array($this, 'setup_post_types'));
		add_action('init', array($this, 'project_type'));
		add_action('acf/init', array($this, 'acf_init'));
	}

	function setup_post_types(){
		$labels = array(
			'name'                  => _x( 'Projects', 'Project General Name', 'genidaqs' ),
			'singular_name'         => _x( 'Project', 'Project Singular Name', 'genidaqs' ),
			'menu_name'             => __( 'Projects', 'genidaqs' ),
			'name_admin_bar'        => __( 'Project', 'genidaqs' ),
			'archives'              => __( 'Project Archives', 'genidaqs' ),
			'attributes'            => __( 'Project Attributes', 'genidaqs' ),
			'parent_item_colon'     => __( 'Parent Project:', 'genidaqs' ),
			'all_items'             => __( 'All Projects', 'genidaqs' ),
			'add_new_item'          => __( 'Add New Project', 'genidaqs' ),
			'add_new'               => __( 'Add New', 'genidaqs' ),
			'new_item'              => __( 'New Project', 'genidaqs' ),
			'edit_item'             => __( 'Edit Project', 'genidaqs' ),
			'update_item'           => __( 'Update Project', 'genidaqs' ),
			'view_item'             => __( 'View Project', 'genidaqs' ),
			'view_items'            => __( 'View Projects', 'genidaqs' ),
			'search_items'          => __( 'Search Project', 'genidaqs' ),
			'not_found'             => __( 'Not found', 'genidaqs' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'genidaqs' ),
			'featured_image'        => __( 'Featured Image', 'genidaqs' ),
			'set_featured_image'    => __( 'Set featured image', 'genidaqs' ),
			'remove_featured_image' => __( 'Remove featured image', 'genidaqs' ),
			'use_featured_image'    => __( 'Use as featured image', 'genidaqs' ),
			'insert_into_item'      => __( 'Insert into Project', 'genidaqs' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Project', 'genidaqs' ),
			'items_list'            => __( 'Projects list', 'genidaqs' ),
			'items_list_navigation' => __( 'Projects list navigation', 'genidaqs' ),
			'filter_items_list'     => __( 'Filter items list', 'genidaqs' ),
		);
		$args = array(
			'label'                 => __( 'Project', 'genidaqs' ),
			'description'           => __( 'Project Description', 'genidaqs' ),
			'labels'                => $labels,
			'supports'              => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', ),
			'menu_icon'              => 'dashicons-location-alt',
			'taxonomies'            => array( 'project_type' ),
			'hierarchical'          => false,
			'public'                => true,
			'show_ui'               => true,
			'show_in_menu'          => true,
			'menu_position'         => 5,
			'show_in_admin_bar'     => true,
			'show_in_nav_menus'     => true,
			'can_export'            => true,
			'has_archive'           => true,
			'exclude_from_search'   => false,
			'publicly_queryable'    => true,
			'capability_type'       => 'page',
		);
		register_post_type( 'projects', $args );
	}

	function project_type() {

		$labels = array(
			'name'                       => _x( 'Project Types', 'Taxonomy General Name', 'genidaqs' ),
			'singular_name'              => _x( 'Project Type', 'Taxonomy Singular Name', 'genidaqs' ),
			'menu_name'                  => __( 'Project Type', 'genidaqs' ),
			'all_items'                  => __( 'All Items', 'genidaqs' ),
			'parent_item'                => __( 'Parent Item', 'genidaqs' ),
			'parent_item_colon'          => __( 'Parent Item:', 'genidaqs' ),
			'new_item_name'              => __( 'New Item Name', 'genidaqs' ),
			'add_new_item'               => __( 'Add New Item', 'genidaqs' ),
			'edit_item'                  => __( 'Edit Item', 'genidaqs' ),
			'update_item'                => __( 'Update Item', 'genidaqs' ),
			'view_item'                  => __( 'View Item', 'genidaqs' ),
			'separate_items_with_commas' => __( 'Separate items with commas', 'genidaqs' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'genidaqs' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'genidaqs' ),
			'popular_items'              => __( 'Popular Items', 'genidaqs' ),
			'search_items'               => __( 'Search Items', 'genidaqs' ),
			'not_found'                  => __( 'Not Found', 'genidaqs' ),
			'no_terms'                   => __( 'No items', 'genidaqs' ),
			'items_list'                 => __( 'Items list', 'genidaqs' ),
			'items_list_navigation'      => __( 'Items list navigation', 'genidaqs' ),
		);
		$args = array(
			'labels'                     => $labels,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'show_in_rest'               => true,
			'rest_base'                  => 'project_type',
		);
		register_taxonomy( 'project_type', array( 'projects' ), $args );

	}

	function acf_init() {
		acf_update_setting('google_api_key', 'AIzaSyA9fUuuTKSvyyJhyF_tlisMtYh8QXS9fz8');
		acf_add_local_field_group(array(
			'key' => 'group_5a26129593121',
			'title' => 'Project Meta Data',
			'fields' => array(
				array(
					'key' => 'field_5a26129e68dbe',
					'label' => 'Location',
					'name' => 'location',
					'type' => 'google_map',
					'instructions' => '',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array(
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'center_lat' => '-121.6173652',
					'center_lng' => '38.5533533',
					'zoom' => 12,
					'height' => '',
				),
			),
			'location' => array(
				array(
					array(
						'param' => 'post_type',
						'operator' => '==',
						'value' => 'post',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => 'default',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));
	}


	function setup_endpoints(){
		
	}


}

new Genidaqs();