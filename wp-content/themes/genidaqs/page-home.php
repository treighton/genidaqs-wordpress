<?php
/**
 * Template Name: Home
 */

$args = [
	'post_type' => 'projects',
	'posts_per_page' => 5
];

$context = Timber::get_context();
$post = new TimberPost();
$projects = Timber::get_posts($args);
$context['post'] = $post;
$context['projects'] = $projects;
Timber::render( array( 'page-home.twig' ), $context );