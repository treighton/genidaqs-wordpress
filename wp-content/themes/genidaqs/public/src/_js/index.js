const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');

let map = document.querySelector('#project-map');
const projects = document.querySelectorAll('.project');
const projectNav = Array.from(document.querySelectorAll('.nav_dot'));
const navToggle = document.querySelector('#nav-toggle');

Math.easeInOutQuad = function (t, b, c, d) {
    t /= d/2;
    if (t < 1) return c/2*t*t + b;
    t--;
    return -c/2 * (t*(t-2) - 1) + b;
};
function removeActive(){
    const activeItem = document.querySelector('.active');
    if(!activeItem) return;
    activeItem.classList.remove('active');
}
if(map){
    const points = Array.from(document.querySelectorAll('.project'));

    const geo = points.map( point => {
        const lat = point.dataset.lat || 38.913;
        const lng = point.dataset.long || -77.032;
        return {
            type: 'Feature',
            geometry: {
                type: 'Point',
                coordinates: [lng, lat]
            },
            properties: {
                title: point.dataset.id,
                description: point.dataset.title
            }
        }
    } );

    const geojson = {
        type: 'FeatureCollection',
        features: [...geo]
    };

    mapboxgl.accessToken = 'pk.eyJ1IjoidHJlaWdodG9uIiwiYSI6ImNpb25rMDVhZTAwMDV2OWt2ZW5rbDF4aGcifQ.pqTlbSrFnCJkkRGd6E4twA';
    map = new mapboxgl.Map({
      container: map,
      style: "mapbox://styles/treighton/cj9ngf3ts3f4p2snost2ms9cm",
      center: [-121.5829968, 38.561457], // starting position [lng, lat]
      zoom: 6,
      pitch: 60, // pitch in degrees
      bearing: 0 // bearing in degrees // starting zoom
    });

    geojson.features.forEach(function(marker) {
        // create a HTML element for each feature
        const markerDiv = document.createElement('div');
        markerDiv.className = 'marker';
        markerDiv.dataset.desc = marker.geometry.coordinates
        markerDiv.addEventListener('click', () => {
            removeActive();
            const targetProject = document.querySelector(`[data-id="${marker.properties.title}"]`);
            const projTop = targetProject.offsetTop;
            const projContainer = document.querySelector('#scroll-container');
            if(projContainer){
                scrollTo(projContainer, projTop - 30, 600);
                targetProject.classList.add('active')
            }else{
                targetProject.scrollIntoView({ behavior: "instant"});
            }       
            map.flyTo({
                center: [
                    targetProject.dataset.long,
                    targetProject.dataset.lat
                ]
            });
        }
        )

        let popup = new mapboxgl.Popup({
            closeButton: false,
        })
            .setHTML(`<h5 style="max-width: 300px;">${marker.properties.description}</h5>`)

        
        // make a marker for each feature and add to the map
        new mapboxgl.Marker(markerDiv)
            .setLngLat(marker.geometry.coordinates)
            .setPopup(popup)
            .addTo(map)
    });
}

function scrollTo(element, to, duration) {
    let start = element.scrollTop,
        change = to - start,
        currentTime = 0,
        increment = 20;

    let animateScroll = function(){
        currentTime += increment;
        let val = Math.easeInOutQuad(currentTime, start, change, duration);
        element.scrollTop = val;
        if(currentTime < duration) {
            setTimeout(animateScroll, increment);
        }
    };
    animateScroll();
}

if(projectNav){
    projectNav.forEach(
        dot => dot.addEventListener('click', dot => {
            const el = dot.target;
            const currentActive = el.parentNode.querySelector('.active');
            
            if(!el.classList.contains('active') && currentActive){
                currentActive.classList.remove('active');
                
            }
            el.classList.add("active");
            const projId = el.dataset.project;
            const targetProject = document.querySelector(`[data-id="${projId}"]`);
            const projTop = targetProject.offsetTop;
            const projContainer = document.querySelector('#scroll-container');
            scrollTo(projContainer,projTop-30, 600);
            map.flyTo({
                center: [
                    targetProject.dataset.long,
                    targetProject.dataset.lat
                    ]
            });
        } )
    )
}

navToggle.addEventListener("click", function () {
    document.body.classList.toggle('showMobileNav');
    this.classList.toggle("active");
});