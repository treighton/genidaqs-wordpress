// Gulp Config
let gulp = require('gulp'),
    newer = require('gulp-newer'),
    imagemin = require('gulp-imagemin'),
    sass = require('gulp-sass'),
    postcss = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    mqpacker = require('css-mqpacker'),
    cssnano = require('cssnano'),
    sourcemaps = require('gulp-sourcemaps'),
    moment = require('moment'),
    header = require('gulp-header'),
    folders = {
        src: 'src/',
        build: 'build/'
    }
;

const gulpStylelint = require('gulp-stylelint');
const package = require('./package.json');



//image processing
gulp.task('images', function () {
    var out = folders.build + '_images/';
    return gulp.src(folders.src + '_images/**/*')
        .pipe(newer(out))
        .pipe(imagemin({optimizationLevel: 5}))
        .pipe(gulp.dest(out))
});

// CSS processing
gulp.task('css', function() {

    const postCssOpts = [
        autoprefixer({ browsers: ['last 2 versions', '> 2%'] }),
        mqpacker
    ];

    return gulp.src(folders.src + '_scss/main.scss')
        .pipe(sass({
            outputStyle: 'nested',
            imagePath: 'images/',
            precision: 3,
            errLogToConsole: true
        }))
        .pipe(postcss(postCssOpts))
        .pipe(gulp.dest(folders.build + 'css/'));

});


// Create style.css with theme header
gulp.task('theme', function () {
    const timestamp = moment().unix();
    const banner = {
        full : '/* Used in my CSS and JS files */',
        min : '/* Used in my minified CSS and JS files */',
        theme :
            `/**
             * Theme Name: ${ package.name} v${ timestamp } 
             * Description: ${ package.description }
             * Version: ${ timestamp }
             * Author: ${ package.author }
             * Author URI: ${ package.repository.url }
             * License: ${ package.license }
             * License URI: ${ package.author }/mit/n
             */`
    };
    return gulp.src('./style.css')
        .pipe(header(banner.theme, { package : package }))
        .pipe(gulp.dest('../'));
});

gulp.task('watch', function () {
    gulp.watch(folders.src + '_scss/**/*', ['css', 'theme']);
    gulp.watch(folders.src + '_images/**/*', ['images']);
});

gulp.task('run', ['css', 'images', 'theme']);

gulp.task('default', ['run', 'watch']);